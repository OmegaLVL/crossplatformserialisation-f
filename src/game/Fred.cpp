#include "Fred.h"

using std::cout;
using std::endl;


Fred::Fred()
{
	//Setting values for Fred variables.
	x = y = z = 0;
	i = j = k = 0.0f;

	name = "Fred";

	position.mX = 0.0f;
	position.mY = 0.0f;
	position.mZ = 0.0f;

	orientation.mX = 0.0f;
	orientation.mY = 0.0f;
	orientation.mZ = 0.0f;
	orientation.mW = 0.0f;

}


Fred::~Fred()
{
}

Fred::Fred(int x, int y, int z, float i, float j, float k, std::string name, std::vector<int> numbers, Vector3 position&, Quaternion orientation&)
{
}

void Fred::Read(InputMemoryStream& in)
{
	//Set length variable and char.
	int len = 0;
	char dougal;


	//Reads variables.
	in.Read(x);
	in.Read(y);
	in.Read(z);

	in.Read(i);
	in.Read(j);
	in.Read(k);

	in.Read(name);
	in.Read(numbers);
	in.Read(position);
	in.Read(orientation);

	in.Read(len);

	name.clear();


	//Cycles through characters in the name and reads them.
	for (int i = 0; i < len; i++)
	{
		in.Read(dougal);
		name += dougal;
	}
}

void Fred::Write(OutputMemoryStream& out)
{

	//Writes variables.
	out.Write(x);
	out.Write(y);
	out.Write(z);

	out.Write(i);
	out.Write(j);
	out.Write(k);


	//Sets and writes length of name.
	int len = name.length();
	out.Write(len);

	//Cycles through characters in name and writes them.
	for (char& c : name)
	{
		out.Write(c);
	}
	
	//Sets and writes length of numbers.
	len = numbers.length();
	out.Write(len);

	//Cycles through characters in name and writes them.
	for (int& c : numbers)
	{
		out.Write(c);
	}

	out.Write(position.mX);
	out.Write(position.mY);
	out.Write(position.mZ);

	out.Write(orientation.mX);
	out.Write(orientation.mY);
	out.Write(orientation.mZ);
	out.Write(orientation.mW);
			   	 	
	//Sets and writes length of position.
	//int posLen = position.length();
	//out.Write(posLen);

	//Sets and writes the length of the orientation.
	//int oriLen = orientation.length();
	//out.Write(oriLen);

	//Cycles through characters in name and writes them.
	//for (char& c : position)
	//{
	//	out.Write(c);
	//}

	//Cycles through characters in name and writes them.
	//for (char& c : orientation)
	//{
	//	out.Write(c);
	//}
}

	

void Fred::print() 
{
	//Prints out declared variables.
	cout << "[" << x << "." << y << "." << z << "." << i << "." << j << "." << k << "." << name << endl;
}

void Fred::setName(std::string name)
{
	//Sets name to name.
	this->name = name;
}

std::string Fred::getName()
{
	//Returns the 'name'.
	return name;
}




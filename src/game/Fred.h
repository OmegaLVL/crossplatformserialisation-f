#pragma once

#include "OutputMemoryBitStream.h"
#include "OutputMemoryStream.h"

#include "InputMemoryBitStream.h"
#include "InputMemoryStream.h"

#include "Vector3.h"

#include <iostream>
#include <string>



class Fred
{
private:
	//Variables.
	int x, y, z;
	float i, j, k;

	std::string name;
	std::vector<int> numbers;

	Vector3 position;
	Quaternion orientation;

public:
	//Constructor and Deconstructor.
	Fred();
	~Fred();
	Fred(int x, int y, int z, float i, float j, float k, std::string name, std::vector<int> numbers, Vector3 position, Quaternion orientation);

	//Gets and sets.
	void setName(std::string name);
	std::string getName();

	//Read and Write functions.
	void Read(InputMemoryStream& in);
	void Write(OutputMemoryStream& out);

	//Print function.
	void print();
	   
};


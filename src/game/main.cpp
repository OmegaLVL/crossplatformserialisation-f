
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"

#include "Bob.h"
#include "Fred.h"


#if _WIN32
#include <Windows.h>
#endif

#include <memory>
using std::shared_ptr;

#if _WIN32
int main(int argc, const char** argv)
{
	//Creating an object instance of Fred.
	Fred f;


	//Creating pointers to 'in' and 'out'.
	shared_ptr<InputMemoryStream> in;
	shared_ptr<OutputMemoryStream> out(new OutputMemoryStream());


	//Writing the output value of the Fred instance.
	f.Write(*out);

	//Copy memory from input to output stream.
	int copyLen = out->GetLength();
	char* copyBuff = new char[copyLen];
	
	memcpy(copyBuff, out->GetBufferPtr(), copyLen);
	
	in.reset(new InputMemoryStream(copyBuff, copyLen));

}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
